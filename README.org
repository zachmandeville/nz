#+TITLE: ii.nz

* Infrastructure for innovation
#+begin_quote
We design and develop new software solutions that achieve amazing things through collaboration with business, government, and community groups.
#+end_quote

** Pairing
Bring this site up on a Pair instance with
#+begin_src tmate :window ii-nz
export SHARINGIO_PAIR_SET_HOSTNAME=ii-nz
hugo serve -b https://ii-nz.$SHARINGIO_PAIR_BASE_DNS_NAME/ --appendPort=false --bind 0.0.0.0 -p 443
#+end_src
