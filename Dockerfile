FROM klakegg/hugo:0.81.0-ext-alpine-ci AS build
ARG HUGO_ENV=production
WORKDIR /site
COPY . .
RUN hugo

# TODO presentation pipeline
# FROM registry.gitlab.com/humacs/humacs/humacs:2021.07.09.0503 AS prezzie-render
# COPY --chown=ii --from=build /site /site
# USER ii
# WORKDIR /site
# RUN for PREZ in ./presentations/*; do ./hack/render-presentation.sh "$(basename ${PREZ})"; done

ARG ARCH=amd64
FROM registry.gitlab.com/safesurfer/go-http-server:1.5.0-$ARCH
ENV APP_SERVE_FOLDER=/app/dist \
    APP_TEMPLATE_MAP_PATH=/app/map.yaml \
    APP_HEADER_SET_ENABLE=true \
    APP_HEADER_MAP_PATH=/app/headers.yaml
LABEL maintainer="ii <maintainer@ii.coop>"
COPY --from=build /site/public /app/dist
COPY presentations /app/dist/presentations
#COPY --from=prezzie-render presentations /app/dist/presentations
COPY template-map.yaml /app/map.yaml
COPY template-headers.yaml /app/headers.yaml
