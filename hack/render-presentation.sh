#!/usr/bin/env bash

GIT_ROOT="$(git rev-parse --show-toplevel 2> /dev/null)"
GIT_ROOT="${GIT_ROOT:-$PWD}"
EL_LOCATION="$HOME/.emacs.d"
if [ "${HUMACS_CONTAINER}" = "yes" ]; then
    EL_LOCATION=/var/local/humacs/doom-emacs
fi
PREZZIE="${1:-}"
if [ ! -f "${GIT_ROOT}/presentations/${PREZZIE}/presentation.org" ]; then
    echo "Unable to find prezzie '${PREZZIE}'" > /dev/stderr
    echo "Available presentations:"
    TOTAL=0
    for PREZZO in "${GIT_ROOT}"/presentations/*; do
        echo "  - $(basename ${PREZZO})"
        TOTAL=$((TOTAL+=1))
    done
    echo "Total: ${TOTAL}"
    exit 1
fi

cd "${GIT_ROOT}/presentations/${PREZZIE}/"
rm -f *.html
time (
    echo "Running emacs batch job"
    echo
    emacs "./presentation.org" --batch -l "${EL_LOCATION}/early-init.el" -l "${EL_LOCATION}/init.el" -f org-re-reveal-export-to-html
    mv presentation.html admin.html
    mv presentation_client.html index.html
)
